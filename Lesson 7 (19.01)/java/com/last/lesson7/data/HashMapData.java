package com.last.lesson7.data;

import java.util.ArrayList;
import java.util.HashMap;

public class HashMapData {

    public HashMap<String, ArrayList<String>> hmData = new HashMap<String, ArrayList<String>>();

    ArrayList<String> name = new ArrayList<String>();
    ArrayList<String> pass = new ArrayList<String>();
    public HashMapData(){
        name.add("Superman");
        name.add("Spiderman");
        name.add("Batman");
        name.add("Xmen");
        pass.add("abc");
        pass.add("123");
        pass.add("efg");
        pass.add("456");

        hmData.put("USERNAME", name);
        hmData.put("PASSWORD", pass);
    }
}
