package com.last.lesson7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        findViewById(R.id.submit).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent loginInfo = new Intent(MainActivity.this, UserCheck.class);
                loginInfo.putExtra("USERNAME", username.getText().toString());
                loginInfo.putExtra("PASSWORD", password.getText().toString());
                startActivity(loginInfo);
            }
        });

        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                username.setText(null);
                password.setText(null);
            }
        });
    }
}
