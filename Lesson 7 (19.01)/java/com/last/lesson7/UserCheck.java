package com.last.lesson7;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.last.lesson7.data.ArrayData;
import com.last.lesson7.data.HashMapData;
import com.last.lesson7.data.ListData;

public class UserCheck extends AppCompatActivity {

    static String user, pass;
    boolean accessAllow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_check);

        user = getIntent().getExtras().getString("USERNAME");
        pass = getIntent().getExtras().getString("PASSWORD");

        TextView welcome = (TextView)findViewById(R.id.welcome);

        accessAllow = checkUserInfoByHashMap();

        Toast.makeText(UserCheck.this, "User: " + user + "\nPass: " + pass, Toast.LENGTH_LONG).show();

        if(accessAllow){
            welcome.setText("Welcome\n" + user);
        }else{
            welcome.setText("Access\nDenied");
            welcome.setTextColor(Color.RED);
        }

        findViewById(R.id.logout).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean checkUserInfoByArray() {
        ArrayData checking = new ArrayData();

        for(int i=0; i < checking.name.length; i++){
            if(checking.name[i].equals(user) && checking.pass[i].equals(pass)){
                return true;
            }
        }
        return false;
    }

    private boolean checkUserInfoByList() {
        ListData checking = new ListData();

        for(int i=0; i < checking.name.size(); i++){
            if(checking.name.get(i).equals(user) && checking.pass.get(i).equals(pass)){
                return true;
            }
        }
        return false;
    }

    private boolean checkUserInfoByHashMap() {

        HashMapData checking = new HashMapData();

        for(int i=0; i < checking.hmData.get("USERNAME").size(); i++){
            if(user.equals(checking.hmData.get("USERNAME").get(i))
                    && pass.equals(checking.hmData.get("PASSWORD").get(i))){
                return true;
            }
        }
        return false;
    }
}
