package com.last.lesson8;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class BFragment extends Fragment {


    public BFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup main_container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_b, main_container, false);

        final TextView total = (TextView) layout.findViewById(R.id.total);

        Integer recieveInfo = null;
        Bundle bundle = getArguments();
        if (bundle != null) {
            recieveInfo = bundle.getInt("SUM");
        }
        total.setText("Total is " + recieveInfo);

        layout.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendData = new Intent(getContext(), SecondActivity.class);
                sendData.putExtra("INFO", total.getText().toString());
                startActivity(sendData);
            }
        });

        return layout;
    }

}
