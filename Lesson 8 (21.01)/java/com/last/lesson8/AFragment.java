package com.last.lesson8;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class AFragment extends Fragment {


    public AFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup main_container,
                             Bundle savedInstanceState) {
        final View layout = inflater.inflate(R.layout.fragment_a, main_container, false);

        layout.findViewById(R.id.countBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText num1 = (EditText) layout.findViewById(R.id.num1);
                EditText num2 = (EditText) layout.findViewById(R.id.num2);

                int n1 = Integer.parseInt(num1.getText().toString());
                int n2 = Integer.parseInt(num2.getText().toString());

                final int sum = n1 + n2;

                BFragment bFrag =  new BFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("SUM", sum);
                bFrag.setArguments(bundle);
                getFragmentManager().beginTransaction().replace(R.id.main_container, bFrag).commit();
            }
        });

        return layout;
    }

}
