package asm4;

import java.util.HashMap;

public class HashMapData {
	
	HashMap<String, String> hm = new HashMap<String, String>();
	
	HashMapData(){
		hm.put("Superman", "abc");
		hm.put("Spiderman", "123");
		hm.put("Batman", "efg");
		hm.put("Xmen", "456");
	}	
}
