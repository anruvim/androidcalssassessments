package asm4;

import java.util.Scanner;

public class Main {
        
    public String name;
    public String pass;
    private static int logSucsed = 0;
    static Scanner s = new Scanner(System.in);
	
    public static void main(String[] args) {
        
        while (logSucsed == 0){
            System.out.println("Input Name: ");
            String name = s.nextLine();
            System.out.println("Input Pass: ");
            String pass = s.nextLine();

            Login log = new Login(name, pass);

            logSucsed = log.check();
        }      
    }
}