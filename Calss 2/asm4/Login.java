package asm4;

public class Login{
    public String name;
    public String pass;
    private Boolean loginPassed = false;
    
    public Login(String n, String p){
        this.name = n;
        this.pass = p;
        
    }
    
    public int check(){
        System.out.println();
        HashMapData checking = new HashMapData();
   
        for(int i=0; i < checking.hm.size(); i++){
        	if( checking.hm.containsKey(name) && checking.hm.get(name).equals(pass)){
        		loginPassed = true;
        		break;
        	}
        }
        
        if(loginPassed){
            System.out.println("Welcome " + name);
            return 1;
        }
        else{
            System.err.println("Wrong name or password!");
            System.out.println();
            return 0;
        }
    }
} 