package asm2;

public class Login{
    public String name;
    public String pass;
    private Boolean loginPassed = false;
    
    public Login(String n, String p){
        this.name = n;
        this.pass = p;
        
    }
    
    public int check(){
        System.out.println();
        ArrayData checking = new ArrayData();
        
        for(int i=0; i < checking.name.length; i++){
        	if(checking.name[i].equals(name) && checking.pass[i].equals(pass)){
        		loginPassed = true;
        		break;
        	}
        }
        
        if(loginPassed){
            System.out.println("Welcome " + name);
            return 1;
        }
        else{
            System.err.println("Wrong name or password!");
            System.out.println();
            return 0;
        }
    }
} 