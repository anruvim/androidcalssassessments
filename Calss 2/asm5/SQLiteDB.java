package asm5;

import java.sql.*;

public class SQLiteDB {
		
	public static void main(String[] args) {
		//some code I took from here:
		//http://easylab.net.ua/swing-i-j2ee/rabota-bazami-dannyih-v-java-sqlite
		
		try{
			Class.forName("org.sqlite.JDBC");
			Connection bd = DriverManager.getConnection("jdbc:sqlite:users.db");
			System.out.println("Database Connected!");
			
			Statement st = bd.createStatement();
			st.execute("CREATE TABLE 'user' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'name' varchar(10), 'pass' varchar(10);");
			System.out.println("Table Created!");
			st.execute("INSERT INTO 'user' ('name', 'pass') VALUES ('Superman', 'abc')");
			st.execute("INSERT INTO 'user' ('name', 'pass') VALUES ('Spiderman', '123')");
			st.execute("INSERT INTO 'user' ('name', 'pass') VALUES ('Batman', 'efg')");
			st.execute("INSERT INTO 'user' ('name', 'pass') VALUES ('Xmen', '456')");
			System.out.println("Data Inserted!");
			
			ResultSet rs = st.executeQuery("SELECT * FROM user");
			
			while(rs.next())
			{
				int id = rs.getInt("id");
				String  name = rs.getString("name");
				String  pass = rs.getString("pass");
		         System.out.println( "ID = " + id );
		         System.out.println( "Name = " + name );
		         System.out.println( "Pass = " + pass );
		         System.out.println();
			}
			
		}catch ( Exception e ){
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		


	}
}
