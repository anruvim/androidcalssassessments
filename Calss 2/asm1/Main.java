package asm1;

import java.util.Scanner;

public class Main {
	
	static int n1, n2;
    static char operation;
    static Scanner s = new Scanner(System.in);

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		System.out.print("Input Number 1: ");
        n1 = s.nextInt();
        System.out.print("Input Number 2: ");
        n2 = s.nextInt();
        System.out.print("Operation (+ or -): ");
        operation = s.next().charAt(0);
        Son result = new Son(n1, n2, operation);
	}

}