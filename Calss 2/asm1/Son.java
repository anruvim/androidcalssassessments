package asm1;

public class Son implements Father{
	
	int result, n1, n2;
	char operation;
	
	public Son(int n1, int n2, char operation){
		System.out.println();
		System.out.println("You choise " + operation);
		if (operation == '+'){	
			System.out.println("Result: " + (result = add(n1, n2)));
		}else if (operation == '-'){
			System.out.println("Result: " + (result = substract(n1, n2)));
		}else System.out.println("Wring Input!");
	}

	@Override
	public int add(int n1, int n2) {
		int sum = n1 + n2;
		return sum;
	}

	@Override
	public int substract(int n1, int n2) {
		int sum = n1 - n2;
		return sum;
	}

}
