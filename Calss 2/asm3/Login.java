package asm3;

public class Login{
    public String name;
    public String pass;
    private Boolean loginPassed = false;
    
    public Login(String n, String p){
        this.name = n;
        this.pass = p;
        
    }
    
    public int check(){
        System.out.println();
        ListData checking = new ListData();
        
        for(int i=0; i < checking.name.size(); i++){
        	if(checking.name.get(i).equals(name) && checking.pass.get(i).equals(pass)){
        		loginPassed = true;
        		break;
        	}
        }
        
        if(loginPassed){
            System.out.println("Welcome " + name);
            return 1;
        }
        else{
            System.err.println("Wrong name or password!");
            System.out.println();
            return 0;
        }
    }
} 