// This is not about anonymous inner class. Go to the website below. Try again. You are doing good.
// http://www.programmerinterview.com/index.php/java-questions/java-anonymous-class-example/

package asm4;

import asm3.Cal;

public class SonCal {
	
	public int result;
	
	public SonCal(int n1, int n2, char o){
		FatherCal equation = new FatherCal(){
			public void add(){
				result = n1 + n2;
			}
			public void substract(){
				result = n1 - n2;
			}
		};
		
		System.out.println();
		System.out.println("You choise " + operation);
		if (operation == '+'){
			equation.add();
			System.out.println("Result: " + result);
		}else if (operation == '-'){
			equation.substract();
			System.out.println("Result: " + result);
		}else System.out.println("Wring Input!");
	}
}
