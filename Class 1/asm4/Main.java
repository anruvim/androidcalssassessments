package asm4;

import java.util.Scanner;

public class Main {
	
	static int n1, n2;
    static char operation;
    static Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.print("Input Number 1: ");
        n1 = s.nextInt();
        System.out.print("Input Number 2: ");
        n2 = s.nextInt();
        System.out.print("Operation (+ or -): ");
        operation = s.next().charAt(0);
        SonCal result = new SonCal(n1, n2, operation);
	}

}