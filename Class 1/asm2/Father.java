package asm2;

public class Father {
	
	private int sum;
	
	public int add(int n1, int n2){
		sum = n1 + n2;
		return sum;
	}
	
	public int substract(int n1, int n2){
		sum = n1 - n2;
		return sum;
	}
}
