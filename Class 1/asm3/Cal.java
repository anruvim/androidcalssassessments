package asm3;

public class Cal{
	
	int result;
	int n1, n2;
	char operation;
	
	
	public Cal(int n1, int n2, char o){
		operation = o;
		System.out.println();
		System.out.println("You choise " + o);
		if (o == '+'){	
			System.out.println("Result: " + (result = Cal.Equations.add(n1, n2)));
		}else if (o == '-'){
			System.out.println("Result: " + (result = Cal.Equations.substract(n1, n2)));
		}else System.out.println("Wring Input!");
	}
	
	public static class Equations {	
		
		private static int sum;
		
		public static int add(int n1, int n2){
			sum = n1 + n2;
			return sum;
		}
		public static int substract(int n1, int n2){
			sum = n1 - n2;
			return sum;
		}
	}
}
