package asm1;

public class Login{
    private final String rightName = "Superman";
    private final String rightPass = "abc123";
    
    public String name;
    public String pass;
    
    
    public Login(String n, String p){  // Jason: String name, String pass
        this.name = n;  // Jason: this.name = name
        this.pass = p;  // Jason: this.pass = pass
        
    }
    
    public int check(){
        System.out.println();
        if(name.equals(rightName) && pass.equals(rightPass)){
            System.out.println("Welcome " + name);
            return 1;
        }
        else{
            System.err.println("Wrong name or password!");
            System.out.println();
            return 0;
        }
    }
} 