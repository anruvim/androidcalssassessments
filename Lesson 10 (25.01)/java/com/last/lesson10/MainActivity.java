package com.last.lesson10;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        Data data = new Data();
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(new MyBaseAdapter(data, this));
    }

    public class MyBaseAdapter extends BaseAdapter {

        Data data;
        Context context;
        LayoutInflater layoutInflater;


        public MyBaseAdapter(Data data, Context context) {
            super();
            this.data = data;
            this.context = context;
            layoutInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return data.name.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View listLayout= layoutInflater.inflate(R.layout.listviewalyout, null);

            ImageView pic = (ImageView) listLayout.findViewById(R.id.pic);
            pic.setImageResource(data.pic.get(position));

            TextView name=(TextView)listLayout.findViewById(R.id.name);
            name.setText(data.name.get(position));

            TextView phone=(TextView)listLayout.findViewById(R.id.phone);
            phone.setText(data.phone.get(position));

            return listLayout;
        }

    }

}
