package com.last.myschoolcontacts;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseHelper extends SQLiteOpenHelper {
    private final String Phone_PATH = "/data/data/com.last.myschoolcontacts/databases/";
    private static final int DB_VERSION = 1;
    private final String DB_NAME;
    private final String DB_PATH;
    private final Context context;
    private SQLiteDatabase db;

    // Connect to Database
    public DataBaseHelper(Context context, String dbName) {
        super(context, dbName, null, DB_VERSION);  // 1 = database version

        this.DB_NAME = dbName;
        this.context = context;
        this.DB_PATH = Phone_PATH + DB_NAME;

        try {
            dbConnect();  // Connect to DB
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Check Database -> Open or Copy
    public void dbConnect() throws IOException {

        boolean dbExist = dbCheck();  // Check DB in /data/data/...

        if (dbExist) {
            /*Database exists: Do not copy*/

        } else {  // No DB in /data/data/...

            this.getReadableDatabase();

            try {
                dbCopy();  // Copy DB from assets to /data/data/
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    // Check DB in /data/data/... and Open Database
    public boolean dbCheck() {

        try {
            db = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);  // Open DB

        } catch (SQLiteException e) {
        }

        if (db != null) {
            // resource leaks prevention
            db.close();
        }
        return db != null ? true : false;
    }

    // Copy DB from assets to Android device (/data/data/...)
    public void dbCopy() throws IOException {

        // DB in the assets (Read)
        InputStream myInput = context.getAssets().open(DB_NAME);

        // Create DB (Write)
        OutputStream myOutput = new FileOutputStream(DB_PATH);

        // Copying DB to /data/data/...
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    /**
     * Getting user data from database
     */
    public HashMap<String, ArrayList<String>> getRows(String type) {

        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> phone = new ArrayList<>();
        ArrayList<String> email = new ArrayList<>();
        ArrayList<String> pic = new ArrayList<>();

        HashMap<String, ArrayList<String>> items = new HashMap<>();

        db = getReadableDatabase();  // Get DB
        Cursor row = db.rawQuery("SELECT * FROM contacts WHERE type =?", new String [] {String.valueOf(type)});  // Get Row

        while (row.moveToNext()) {
            name.add(row.getString(1));
            phone.add(row.getString(2));
            email.add(row.getString(3));
            pic.add(row.getString(4));
        }
        items.put("NAME", name);
        items.put("PHONE", phone);
        items.put("EMAIL", email);
        items.put("PIC", pic);

        row.close();
        db.close();

        return items;
    }

    public boolean addRow(String intName, String intPhone, String intEmail, String intType){
        ContentValues values = new ContentValues();
        values.put("name", intName);
        values.put("phone", intPhone);
        values.put("email", intEmail);
        values.put("type", intType);
        db = getWritableDatabase();
        long result = db.insert("contacts", null, values);
        db.close();
        if(result>0){
            Log.d("result", String.valueOf(result));
            return true;
        }else return false;
    }

    @Override
    public synchronized void close() {
        if (db != null)
            db.close();
        super.close();
    }

    @Override  // create new tables
    public void onCreate(SQLiteDatabase db) {
    }

    @Override  // drop old tables and create new tables
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

