package com.last.myschoolcontacts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class AddingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding);

        final Spinner spinner = (Spinner)findViewById(R.id.intType);

        ArrayAdapter adapter = ArrayAdapter.createFromResource(this,
                R.array.intType, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);

        findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText intName = (EditText) findViewById(R.id.intName);
                EditText intPhone = (EditText) findViewById(R.id.intPhone);
                EditText intEmail = (EditText) findViewById(R.id.intEmail);
                Spinner intType = (Spinner) findViewById(R.id.intType);
                if(intName.getText().length() == 0){
                    Toast.makeText(AddingActivity.this, "Input Name", Toast.LENGTH_SHORT).show();
                }else if(intPhone.getText().length() == 0){
                    Toast.makeText(AddingActivity.this, "Input Phone", Toast.LENGTH_SHORT).show();
                }else if(intEmail.getText().length() == 0){
                    Toast.makeText(AddingActivity.this, "Input Email", Toast.LENGTH_SHORT).show();
                }else{
                    String name = intName.getText().toString();
                    String phone = intPhone.getText().toString();
                    String email = intEmail.getText().toString();
                    String type = intType.getSelectedItem().toString();
                    if(type.equals("Student")){
                        type = "S";
                    }else if (type.equals("Teacher")){
                        type = "T";
                    }else if (type.equals("Administration")){
                        type = "A";
                    }
                    DataBaseHelper db = new DataBaseHelper(AddingActivity.this, "mycontacts");
                    boolean userAdded = db.addRow(name, phone, email, type);
                    if(userAdded){
                        setResult(RESULT_OK);
                        finish();
                    }else {
                        setResult(0);
                    }
                }
            }
        });
    }



}
