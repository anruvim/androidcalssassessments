package com.last.myschoolcontacts;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ruvim on 1/26/2016.
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    HashMap item;
    ArrayList name, phone, email, pic;
    //private final Context context;
    private final View context;

    public MyItemRecyclerViewAdapter(HashMap<String, ArrayList<String>> item, View context) {
        this.item = item;
        this.context = context;
        this.name = item.get("NAME");
        this.phone = item.get("PHONE");
        this.email = item.get("EMAIL");
        this.pic = item.get("PIC");
    }

    /*Layout inflation*/
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_view, parent, false);
        return new ViewHolder(layout);
    }

    /*Bind to widgets*/
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.myName.setText(name.get(position).toString());
        holder.myPhone.setText(phone.get(position).toString());
        holder.myEmail.setText(email.get(position).toString());
        holder.myPic.setImageResource(R.drawable.face);
    }

    /*Looping items*/
    @Override
    public int getItemCount() {
        return name.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView myName;
        public final TextView myPhone;
        public final TextView myEmail;
        public final ImageView myPic;

        public ViewHolder(View layout) {
            super(layout);
            mView = layout;
            myName = (TextView) layout.findViewById(R.id.name);
            myPhone = (TextView) layout.findViewById(R.id.phone);
            myEmail = (TextView) layout.findViewById(R.id.email);
            myPic = (ImageView) layout.findViewById(R.id.pic);
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "Test", Toast.LENGTH_SHORT).show();
                }
            });*/
        }

    }
}