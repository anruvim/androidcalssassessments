package com.last.lesson4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView myName, myPass, showName, showPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void showInfo(View view){
        myName = (TextView) findViewById(R.id.myName);
        myPass = (TextView) findViewById(R.id.myPass);
        showName = (TextView) findViewById(R.id.showPass);
        showPass = (TextView) findViewById(R.id.showName);

        showName.setText(myName.getText());
        showPass.setText(myPass.getText());
    }
}
