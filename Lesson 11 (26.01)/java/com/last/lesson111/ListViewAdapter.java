package com.last.lesson111;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ruvim on 1/26/2016.
 */
public class ListViewAdapter extends BaseAdapter {

    HashMap item;
    ArrayList name, phone, pic;
    Context context;
    LayoutInflater layoutInflater;


    public ListViewAdapter(HashMap<String, ArrayList<String>> item, Context context) {
        super();
        this.item = item;
        this.context = context;
        this.name = item.get("NAME");
        this.phone = item.get("PHONE");
        this.pic = item.get("PIC");
        layoutInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return name.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View listLayout = layoutInflater.inflate(R.layout.list_item, null);

        TextView nameView=(TextView)listLayout.findViewById(R.id.name);
        nameView.setText(name.get(position).toString());

        TextView phoneView=(TextView)listLayout.findViewById(R.id.phone);
        phoneView.setText(phone.get(position).toString());

        return listLayout;
    }

}
