package com.last.lesson111;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

/*        HashMap <String, ArrayList<String>> itemList = new HashMap<>();

        ArrayList<String> name = new ArrayList<>();
        name.add("Superman");
        name.add("Spiderman");
        name.add("Xmen");
        name.add("Batman");
        itemList.put("NAME", name);

        ArrayList<String> phone = new ArrayList<>();
        phone.add("789456");
        phone.add("123456");
        phone.add("741852");
        phone.add("865412");
        itemList.put("PHONE", phone);

        ArrayList<String> pic = new ArrayList<>();
        pic.add(String.valueOf(R.drawable.superman));
        pic.add(String.valueOf(R.drawable.spiderman));
        pic.add(String.valueOf(R.drawable.xmen));
        pic.add(String.valueOf(R.drawable.batman));
        itemList.put("PIC", pic);*/


        DataBaseHelper db = new DataBaseHelper(MainActivity.this, "contacts");
        HashMap <String, ArrayList<String>> itemList = new HashMap<>();
        itemList = db.getRows();

/*      ListView myView = (ListView) findViewById(R.id.myView);
        ListViewAdapter list = new ListViewAdapter(itemList, this);
        myView.setAdapter(list);*/

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.myView);
        recyclerView.setAdapter(new MyItemRecyclerViewAdapter(itemList, MainActivity.this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();



        return super.onOptionsItemSelected(item);
    }
}
