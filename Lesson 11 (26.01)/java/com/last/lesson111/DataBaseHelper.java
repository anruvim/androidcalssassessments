package com.last.lesson111;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class DataBaseHelper extends SQLiteOpenHelper {
    private final String Phone_PATH = "/data/data/com.last.lesson111/databases/";
    private static final int DB_VERSION = 1;
    private final String DB_NAME;
    private final String DB_PATH;
    private final Context context;
    private SQLiteDatabase db;

    // Connect to Database
    public DataBaseHelper(Context context, String dbName) {
        super(context, dbName, null, DB_VERSION);  // 1 = database version

        this.DB_NAME = dbName;
        this.context = context;
        this.DB_PATH = Phone_PATH + DB_NAME;

        try {
            dbConnect();  // Connect to DB
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Check Database -> Open or Copy
    public void dbConnect() throws IOException {

        boolean dbExist = dbCheck();  // Check DB in /data/data/...

        if (dbExist) {
            /*Database exists: Do not copy*/

        } else {  // No DB in /data/data/...

            this.getReadableDatabase();

            try {
                dbCopy();  // Copy DB from assets to /data/data/
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    // Check DB in /data/data/... and Open Database
    public boolean dbCheck() {

        try {
            db = SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READWRITE);  // Open DB

        } catch (SQLiteException e) {
        }

        if (db != null) {
            // resource leaks prevention
            db.close();
        }
        return db != null ? true : false;
    }

    // Copy DB from assets to Android device (/data/data/...)
    public void dbCopy() throws IOException {

        // DB in the assets (Read)
        InputStream myInput = context.getAssets().open(DB_NAME);

        // Create DB (Write)
        OutputStream myOutput = new FileOutputStream(DB_PATH);

        // Copying DB to /data/data/...
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    /**
     * Getting user data from database
     */
    public HashMap<String, ArrayList<String>> getRows() {

        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> phone = new ArrayList<>();

        HashMap<String, ArrayList<String>> items = new HashMap<>();

        String query = "SELECT * FROM IT";

        db = getReadableDatabase();  // Get DB
        Cursor row = db.rawQuery(query, null);  // Get Row

        while (row.moveToNext()) {
            name.add(row.getString(1));
            phone.add(row.getString(2));
        }
        items.put("NAME", name);
        items.put("PHONE", phone);

        row.close();
        db.close();

        return items;
    }

    @Override
    public synchronized void close() {
        if (db != null)
            db.close();
        super.close();
    }

    @Override  // create new tables
    public void onCreate(SQLiteDatabase db) {
    }

    @Override  // drop old tables and create new tables
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
