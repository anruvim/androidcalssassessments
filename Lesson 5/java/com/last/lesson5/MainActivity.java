package com.last.lesson5;

import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView num1, num2, num3, average;
    Snackbar snackbar;
    RadioGroup showTypeRd;
    RadioButton showType;
    //Button count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = (TextView) findViewById(R.id.n1);
        num2 = (TextView) findViewById(R.id.n2);
        num3 = (TextView) findViewById(R.id.n3);
        average = (TextView) findViewById(R.id.average);
        showTypeRd = (RadioGroup) findViewById(R.id.type);
        //count = (Button)findViewById(R.id.avrBut);

        //Toast.makeText(this, "hi", Toast.LENGTH_LONG).show();

        findViewById(R.id.avrBut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n1, n2, n3, avg;
                n1 = Integer.parseInt(num1.getText().toString());
                n2 = Integer.parseInt(num2.getText().toString());
                n3 = Integer.parseInt(num3.getText().toString());
                avg = (n1 + n2 + n3) / 3;
                average.setText(String.valueOf(avg));

                int selectedId = showTypeRd.getCheckedRadioButtonId();
                showType = (RadioButton) findViewById(selectedId);

                if(showType.getText().equals("Toast")) {
                    Toast.makeText(MainActivity.this, "Average is " + String.valueOf(avg), Toast.LENGTH_LONG).show();
                }else if (showType.getText().equals("Snackbar")){
                    Snackbar.make(v, "Average is " + String.valueOf(avg), Snackbar.LENGTH_LONG).show();
                }else if (showType.getText().equals("Alert")){
                    AlertDialog alert = new AlertDialog.Builder(MainActivity.this).create();
                    alert.setTitle("Average");
                    alert.setMessage(String.valueOf(avg));
                    alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alert.show();
                }
                //snackbar.show();
            }
        });
    }
}
