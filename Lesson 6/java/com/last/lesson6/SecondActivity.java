package com.last.lesson6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    public final static String USER = "username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String user = getIntent().getExtras().getString("username");
        String pass = getIntent().getExtras().getString("password");

        final TextView myName = (TextView)findViewById(R.id.myName);
        TextView myPass = (TextView)findViewById(R.id.myPass);

        myName.setText(user);
        myPass.setText(pass);

        findViewById(R.id.backBTN).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent();
                backIntent.putExtra(USER, myName.getText().toString());
                setResult(RESULT_OK, backIntent);
                finish();
            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_one, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {

            case R.id.search:
                Toast.makeText(SecondActivity.this, "Search is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.folder:
                Toast.makeText(SecondActivity.this, "Add is Selected", Toast.LENGTH_SHORT).show();
                return true;

            case R.id.settings:
                Toast.makeText(SecondActivity.this, "Settings is Selected", Toast.LENGTH_SHORT).show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
