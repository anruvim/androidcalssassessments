package com.last.lesson6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener(){
            static final private int USERNAME = 0;
            @Override
            public void onClick(View v) {
                EditText name = (EditText) findViewById(R.id.name);
                EditText pass = (EditText) findViewById(R.id.pass);
                Intent sendInfo = new Intent(MainActivity.this, SecondActivity.class);
                sendInfo.putExtra("username", name.getText().toString());
                sendInfo.putExtra("password", pass.getText().toString());
                startActivityForResult(sendInfo, USERNAME);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            String username = data.getStringExtra(SecondActivity.USER);
            Toast.makeText(MainActivity.this, "Welcome " + username, Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(MainActivity.this, "ERROR!", Toast.LENGTH_LONG).show();
        }
    }

}
