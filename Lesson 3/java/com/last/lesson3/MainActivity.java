package com.last.lesson3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.*;

public class MainActivity extends AppCompatActivity {

    TextView myText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    //Sites that helped me:
    //http://developer.android.com/training/basics/firstapp/starting-activity.html
    //http://stackoverflow.com/questions/22573299/how-can-i-change-the-text-with-a-button-click-android-studio-xml

    public void txtChange(View view) {
        myText = (TextView) findViewById(R.id.textView);
        if(myText.getText().equals("Hi World! by Ruvim")){
            myText.setText("Hello World! by Ruvim");
        }else if(myText.getText().equals("Hello World! by Ruvim")){
            myText.setText("Hi World! by Ruvim");
        }
    }

}
